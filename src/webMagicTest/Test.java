package webMagicTest;

import java.io.IOException;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

public class Test {
	public static void main(String args[]) {
		String url = "http://www.biquge.com/";
		try {
			Document doc = Jsoup.connect(url).get();
			Elements elements = doc.select("#hotcontent .l .item");
			for (Element element : elements) {
				String novelUrl = element.select(".image a").attr("href");
				String imageUrl = element.select(".image a img").attr("src");
				String novelName = element.select(".image a img").attr("alt");
				String authorName = element.select("dl dt span").text();
				System.out.println("小说路径是:" + novelUrl + "   图片路径是:" + imageUrl
						+ "小说名字是:" + novelName + " 作者名字是:" + authorName);
			}
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
}
